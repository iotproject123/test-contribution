# Test de contribution
Ceci est un projet temporaire créer pour s'assurer que tout le monde est capable de suivre les étapes mises en place pour contribuer aux projets du groupe. Pour ce faire, j'ai créer une tâche que tout le monde devra accomplir. Faites cette tâche proprement, car vos contributions ne seront acceptées dans aucuns projets tant que vous ne l'aurez pas réussie. Vous recevrez du feedback d'un administrateur sur votre merge request.

## Tâche obligatoire
Votre but sera donc de modifier le fichier [membres.md](https://gitlab.com/iotproject123/test-contribution/blob/master/membres.md) pour y ajouter votre nom et une chose sur vous ***qui n'est pas reliée à l'électronique***. Basez-vous sur la branche `master` et ajoutez votre partie après celles des autres. Suivez à la lettre le [guide de contribution](https://gitlab.com/iotproject123/contribuer) qui se trouve dans un projet permanent. Allez lire la partie `base de git` pour un rappel sur les commandes de git, la partie `base de gitlab` pour un survol de gitlab et la partie `contribuer` pour le guide de contribution aux projets du groupe que vous devez respecter.

### Détails
+ Utiliser `votre nom` comme titre pour l'`issue` que vous allez créer.
+ Vous n'avez pas besoin d'ajouter de description à l'`issue`.
+ Ajouter le tag `obligatoire` à l'issue.
+ Modifier le fichier membre.md une première fois pour y ajouter votre nom __en titre__.
+ Faire un commit.
+ Modifier le fichier membre.md pour y ajouter qqch sur vous __en texte__.
+ Faire un commit.

### Rappels
N'oubliez pas de cloner le projet localement et d'utiliser des messages pertinents et descriptifs pour vos commits. Si les instructions ne sont pas claires ou vous avez des questions envoyez un message __privé__ à Zakary Kamal Ismail sur Slack (ne pas poser de questions sur le salon général sur cette tâche). N'hesitez pas à poser des questions puisqu'elles peuvent servir à apporter des clarifications au guide de contribution et à ce test. N'oubliez pas que, une fois le `merge request` accepté, il vous reste des étapes à accomplir.

## Tâche optionnelle
Un `issue` à aussi été créé pour corriger les fautes de français dans ce projet et dans le projet de contribution. Vous êtes donc invités à, si vous le voulez, corriger ces fautes en suivant le protocol établi. Lorsque vous ferez votre pour des corrections `merge request`, ne supprimez pas la branche que vous aurez utilisée pour les corrections et ne fermez pas l'`issue` pour les fautes de français. Vous ne devez pas non plus vous assigner cet `issue`.
